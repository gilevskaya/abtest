import * as React from "react";
import * as ReactDOM from "react-dom";
import * as cookie from "js-cookie";
import { HomePage, A_BGCOLOR, B_BGCOLOR, COOKIE_NAME } from "./HomePage";

import { configure, shallow } from "enzyme";
import * as Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });

const BIG_VALUE = 10;

describe("HomePage", () => {
  it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<HomePage />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("should set cookie and page background match the cookie", () => {
    const component = shallow(<HomePage />);
    const cookieBgColor = cookie.get(COOKIE_NAME);
    expect(cookieBgColor).toMatch(new RegExp(`${A_BGCOLOR}|${B_BGCOLOR}`));
    const isBgA = component.is(`.${A_BGCOLOR}`);
    const isBgB = component.is(`.${B_BGCOLOR}`);
    expect(xor(isBgA, isBgB)).toBe(true);
    if (isBgA) expect(cookieBgColor).toMatch(A_BGCOLOR);
    if (isBgB) expect(cookieBgColor).toMatch(B_BGCOLOR);
  });

  it("should persist background for the same user", () => {
    cookie.remove(COOKIE_NAME);
    const count = { a: 0, b: 0 };
    for (let i = 0; i < BIG_VALUE; i++) {
      const component = shallow(<HomePage />);
      if (component.is(`.${A_BGCOLOR}`)) count.a++;
      if (component.is(`.${B_BGCOLOR}`)) count.b++;
    }
    expect(xor(count.a === BIG_VALUE, count.b === BIG_VALUE)).toBe(true);
  });

  it("should change background for different users", () => {
    cookie.remove(COOKIE_NAME);
    const count = { a: 0, b: 0 };
    for (let i = 0; i < BIG_VALUE; i++) {
      const component = shallow(<HomePage />);
      if (component.is(`.${A_BGCOLOR}`)) count.a++;
      if (component.is(`.${B_BGCOLOR}`)) count.b++;
      cookie.remove(COOKIE_NAME);
    }
    expect(xor(count.a === BIG_VALUE, count.b === BIG_VALUE)).not.toBe(true);
  });
});

function xor(a: boolean, b: boolean) {
  return a ? !b : b;
}
