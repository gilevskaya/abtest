import * as React from "react";
import * as cookie from "js-cookie";

export const A_BGCOLOR = "bg-yellow-lighter";
export const B_BGCOLOR = "bg-grey";
export const COOKIE_NAME = "bgcolor";

export class HomePage extends React.Component {
  public state: { bgColor: string };

  public componentWillMount() {
    const userBgColor = cookie.get(COOKIE_NAME);
    if (userBgColor) this.setState({ bgColor: userBgColor });
    else {
      const newUserBgColor = Math.random() * 10 < 5 ? A_BGCOLOR : B_BGCOLOR;
      cookie.set(COOKIE_NAME, newUserBgColor);
      this.setState({ bgColor: newUserBgColor });
    }
  }

  public render() {
    return (
      <div
        id="main-container"
        className={`w-full min-h-screen ${this.state && this.state.bgColor}`}
      >
        <div>HomePage</div>
      </div>
    );
  }
}
