import * as React from "react";
import * as ReactDOM from "react-dom";
import registerServiceWorker from "./utils/registerServiceWorker";

import { HomePage } from "./pages/HomePage/HomePage";

import "./index.css";

ReactDOM.render(<HomePage />, document.getElementById("root") as HTMLElement);
registerServiceWorker();
